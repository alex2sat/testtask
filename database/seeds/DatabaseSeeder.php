<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 5; $i++) {
            $user = \App\User::create([
                'name' => str_random(10),
                'email' => str_random(10).'@test.app',
                'address1' => str_random(10),
                'address2' => str_random(10),
            ]);

            for ($j = 0; $j < 3; $j++) {
                $product = \App\Product::create([
                    'internal_name' => str_random(10),
                    'display_name' => str_random(10),
                ]);

                $licence = \App\Licence::create([
                    'product_id' => $product->id,
                    'key' => hash('sha256', $product->internal_name . $user->email)
                ]);

                \Illuminate\Support\Facades\DB::table('linked_licences')->insert([
                    'licence_id' => $licence->id,
                    'user_id' => $user->id

                ]);
            }
        }
    }
}
