<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LinkedLicencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('linked_licences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('licence_id')->unsigned();
            $table->integer('user_id')->unsigned();
        });

        Schema::table('linked_licences', function (Blueprint $table) {
            $table->foreign('licence_id', 'fl_linked_licenses_licences_licence_id')->references('id')->on('licences');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('linked_licences', function (Blueprint $table) {
            $table->dropForeign('licence_id');
            $table->dropForeign('user_id');
        });

        Schema::dropIfExists('linked_licences');
    }
}
