<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getDetail()
    {
        $id = request('id');
        $user = User::find($id);
        $user || abort(404);

        return $user;
    }

    public function getLicences($id)
    {
        $user = User::where('id', $id)->with('licences')->first();
        $user || abort(404);

        return $user;
    }
}
