<?php

namespace App\Http\Middleware;

use App\Licence;
use App\User;
use Closure;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class OutputApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        $routeName = $request->route()->getName();

        if ($response->status() == 200) {
            $fractal = new Manager();

            if ($routeName == 'user_detail') {
                $user = $response->original;
                $licence = $user->licences->first();
                $product = $licence->product;

                $resource = new Item($user, function(User $user) use ($product) {
                    return [
                        'user_name' => $user->name,
                        'address' => implode(', ', [$user->address1, $user->address2, $user->country]),
                        'membership' => $product->display_name
                    ];
                });

                $response->setContent($fractal->createData($resource)->toJson());
            } elseif ($routeName == 'licences') {
                $user = $response->original;

                $resource = new Collection($user->licences, function(Licence $licence) use ($user) {
                    $product = $licence->product;

                    return [
                        'user_name' => $user->name,
                        'product' => $product->display_name,
                        'licence_key' => hash('sha256', $product->internal_name . $user->email),
                    ];
                });

                $response->setContent($fractal->createData($resource)->toJson());
            }
        }

        return $response;
    }
}
