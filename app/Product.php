<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];

    public function licence()
    {
        return $this->hasOne(Licence::class);
    }
}
